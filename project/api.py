from rest_framework import serializers, viewsets
from .models import Project, ProjectContent, ProjectContentImage, ProjectSpec

class ProjectSpecSeriallizer(serializers.ModelSerializer):
    class Meta:
        model = ProjectSpec
        exclude = ('project', )

class ContentBlockImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectContentImage
        exclude = ('project_content', )


class ContentBlockSerializer(serializers.ModelSerializer):
    images = ContentBlockImageSerializer(source='projectcontentimage_set', many=True)

    class Meta:
        model = ProjectContent
        exclude = ('project',)

class ProjectPreviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ('id', 'title', 'title_en', 'slug', 'preview_image', 'year')

class ProjectSerializer(serializers.ModelSerializer):
    content = ContentBlockSerializer(source='projectcontent_set', many=True)
    specs = ProjectSpecSeriallizer(source='projectspec_set', many=True)
    next_project = ProjectPreviewSerializer()

    class Meta:
        model = Project
        fields = '__all__'

class ProjectViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    lookup_field = 'slug'

class HomepageProjectViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ProjectPreviewSerializer
