from django.http import HttpResponse

from users.models import User


def health_check(request):
    User.objects.first()  # check database connection
    return HttpResponse('OK')
