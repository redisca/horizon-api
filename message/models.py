from django.db import models

class Message(models.Model):
    email = models.CharField('Email', max_length=250)
    name = models.CharField('Имя', max_length=250)
    created_at = models.DateTimeField('Создана', auto_now_add=True)

    class Meta:
        verbose_name = 'Заявка'
        verbose_name_plural = 'Заявки'