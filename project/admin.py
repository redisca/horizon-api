from django.contrib import admin
from .models import Project, ProjectSpec, ProjectContent, ProjectContentImage
import nested_admin
from .forms import ProjectContentForm

class ProjectSpecInline(nested_admin.NestedTabularInline):
    model = ProjectSpec

class ProjectContentImageInline(nested_admin.NestedTabularInline):
    model = ProjectContentImage

class ProjectContentInline(nested_admin.NestedStackedInline):
    model = ProjectContent
    form = ProjectContentForm
    inlines = [
        ProjectContentImageInline
    ]

class ProjectAdmin(nested_admin.NestedModelAdmin):
    inlines = [
        ProjectContentInline,
        ProjectSpecInline
    ]

# Register your models here.
admin.site.register(Project, ProjectAdmin)