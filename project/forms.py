from django import forms
from pagedown.widgets import AdminPagedownWidget
from .models import ProjectContent

class ProjectContentForm(forms.ModelForm):
    text = forms.CharField(widget=AdminPagedownWidget(), required=False)
    text_en = forms.CharField(widget=AdminPagedownWidget(), required=False)

    class Meta:
        model = ProjectContent
        fields = "__all__"
