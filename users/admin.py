from django.contrib import admin
from django.contrib.auth import (
    admin as auth_admin,
    models as auth_models,
)

from .models import User, Group


class UserAdmin(auth_admin.UserAdmin):
    list_display = ('username', 'full_name', 'is_staff')
    search_fields = ('username', 'email', 'first_name', 'last_name')
    filter_horizontal = ('user_permissions', 'groups')
    list_display_links = ('username',)
    ordering = ('id',)
    list_filter = ()

    fieldsets = (
        (None, {
            'fields': ( 'username', 'password'),
        }),
        ('User info', {
            'fields': ('email', 'first_name', 'last_name')
        }),
        ('Permissions', {
            'fields': ('groups', 'is_staff', 'is_superuser'),
        }),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'username',
                'password1',
                'password2',
            ),
        }),
    )

    def full_name(self, obj):
        return obj.get_full_name()


admin.site.unregister(auth_models.Group)
admin.site.register(User, UserAdmin)
admin.site.register(Group, auth_admin.GroupAdmin)
