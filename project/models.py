from django.db import models
from django.core.validators import FileExtensionValidator

class Project(models.Model):
    title = models.CharField('Название', max_length=250)
    title_en = models.CharField('Название (EN)', max_length=250)
    slug = models.CharField('URL Slug', max_length=250, unique=True)
    order = models.IntegerField('Сортировка на главной', default=0)
    year = models.CharField('Год создания', max_length=250)
    preview_image = models.ImageField('Превью', upload_to='projects/previews')
    image_desktop = models.ImageField('Изображение (десктоп)', upload_to='projects/desktop')
    image_mobile = models.ImageField('Изображение (мобайл)', upload_to='projects/mobile')
    specs_title = models.CharField('Характеристики – Название', max_length=250)
    specs_title_en = models.CharField('Характеристики – Название (EN)', max_length=250)
    specs_image = models.ImageField('Характеристики – Изображение', upload_to='projects/specs', validators=[FileExtensionValidator(allowed_extensions=['png', 'gif'])])
    next_project = models.ForeignKey('Project', null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Проект'
        verbose_name_plural = 'Проекты'
        ordering = ['order']

class ProjectContent(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    subtitle = models.CharField('Текст слева', max_length=250, blank=True)
    subtitle_en = models.CharField('Текст слева (EN)', max_length=250, blank=True)
    text = models.TextField('Текст справа', blank=True)
    text_en = models.TextField('Текст справа (EN)', blank=True)
    video_url = models.CharField('Ссылка на Youtube', max_length=250, blank=True)

    class Meta:
        verbose_name = 'Контентый блок'
        verbose_name_plural = 'Контентные блоки'

class ProjectContentImage(models.Model):
    project_content = models.ForeignKey(ProjectContent, on_delete=models.CASCADE)

    order = models.IntegerField('Позиция', blank=True, default=0)
    image = models.ImageField('Изображение', upload_to='projects/content')

    class Meta:
        verbose_name = 'Изображение'
        verbose_name_plural = 'Изображения'
        ordering = ['order']

class ProjectSpec(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)

    title = models.CharField('Название', max_length=250)
    title_en = models.CharField('Название (EN)', max_length=250)
    value = models.CharField('Значение', max_length=250)
    value_en = models.CharField('Значение (EN)', max_length=250)
    order = models.IntegerField('Позиция', blank=True, default=0)

    class Meta:
        verbose_name = 'Характеристика'
        verbose_name_plural = 'Характеристики'
        ordering = ['order']