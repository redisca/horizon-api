#!/usr/bin/env bash

# This file is passed to the --vault-password-file argument if ANSIBLE_VAULT_PASSWORD
# variable is provided. It helps play.sh to use plain-text password directly
# from the environment variable. In general, it makes sense only on CI and not
# necessarily for using it on the localhost.

if [[ $ANSIBLE_VAULT_PASSWORD = "" ]]; then
    echo "You should provide ANSIBLE_VAULT_PASSWORD variable"
    exit 1
fi

echo -n $ANSIBLE_VAULT_PASSWORD
