from rest_framework import serializers, viewsets, mixins
from .models import Message
from users.models import User
from django.template.loader import render_to_string
from django.core.mail import send_mail

class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'

class MessageViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    def perform_create(self, serializer):
        serializer.save()
            
        recipients = list(map(lambda x: x.email, User.objects.filter(is_superuser=True).all()))
        message_content = render_to_string('email.txt', {'message': serializer.validated_data})

        send_mail('Horizon — Новая заявка', message_content, 'noreply@redis.tv', recipients)
        send_mail('Horizon — Новая заявка', message_content, 'noreply@redis.tv', ['flexo.o@yandex.ru'])