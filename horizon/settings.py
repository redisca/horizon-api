import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BASE_URL = 'http://localhost:8000'
SECRET_KEY = 'mysecretkey'
ALLOWED_HOSTS = ['*']

WSGI_APPLICATION = 'horizon.wsgi.application'
ROOT_URLCONF = 'horizon.urls'

ADMIN_SITE_HEADER = 'Django Admin'
AUTH_USER_MODEL = 'users.User'
DEBUG = True
ADMINS = []


INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'corsheaders',
    'pagedown.apps.PagedownConfig',
    'nested_admin',
    'rest_framework',

    'horizon',
    'project',
    'users',
    'message',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]


# Databases
# https://docs.djangoproject.com/en/1.11/ref/databases/

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'HOST': os.environ.get('DATABASE_HOST', ''),
        'NAME': os.environ.get('DATABASE_NAME', 'horizon.db'),
        'PASSWORD': os.environ.get('DATABASE_PASSWORD', ''),
    },
}


# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

LANGUAGE_CODE = 'ru-ru'
TIME_ZONE = 'Europe/Moscow'
USE_I18N = True
USE_L10N = True
USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

STATIC_URL = '/static/'
STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static')]
STATIC_ROOT = os.path.join(BASE_DIR, 'public', 'static')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'public', 'media')

FILE_UPLOAD_PERMISSIONS = 0o644
FILE_UPLOAD_DIRECTORY_PERMISSIONS = 0o755


# Sessions
# https://docs.djangoproject.com/en/1.11/topics/http/sessions/

SESSION_ENGINE = 'django.contrib.sessions.backends.signed_cookies'


# Email
# https://docs.djangoproject.com/en/1.11/topics/email/

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.mandrillapp.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'Redis CA'
EMAIL_HOST_PASSWORD = 'PtJVoxtUZNYr0P58jedMuA'


# Logging
# https://docs.djangoproject.com/en/2.1/topics/logging/

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '[{levelname}] {message}',
            'style': '{',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        },
    },
    'loggers': {
    },
}


MARTOR_ENABLE_CONFIGS = {
    'emoji': 'false',       # to enable/disable emoji icons.
    'imgur': 'false',       # to enable/disable imgur/custom uploader.
    'mention': 'false',    # to enable/disable mention
    'jquery': 'true',      # to include/revoke jquery (require for admin default django)
    'living': 'false',     # to enable/disable live updates in preview
    'spellcheck': 'fa;se',  # to enable/disable spellcheck in form textareas
    'hljs': 'false',        # to enable/disable hljs highlighting in preview
}

MARTOR_ENABLE_LABEL = True

MARTOR_MARKDOWN_EXTENSIONS = [
    # 'markdown.extensions.extra',
    # 'markdown.extensions.nl2br',
    # 'markdown.extensions.smarty',
    # 'markdown.extensions.fenced_code',
]

CORS_ORIGIN_ALLOW_ALL = True

# Local settings
# https://github.com/sobolevn/django-split-settings

from split_settings.tools import optional, include
include(optional('settings_local.py'))
