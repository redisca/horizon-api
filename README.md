# horizon

## Dependencies

 * Python 3.6
 * PostgreSQL 10

## Deployment

For quick deployment (only app code) use:

```
./ansible/play.sh stage deploy --tags quick
```

For full deployment (app and system configs) use:

```
./ansible/play.sh stage deploy
```
