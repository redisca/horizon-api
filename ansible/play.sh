#!/usr/bin/env bash

DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
PLAYBOOK_NAME=$1
INVENTORY_NAME=$2

if [[ $PLAYBOOK_NAME = "" || ${PLAYBOOK_NAME:0:1} = "-" ]]; then
    echo "Shortcut for ansible-playbook"
    echo "Usage: play.sh playbook [inventory] [options]"
    echo "Where:"
    echo "  inventory - name of the inventory file without exnesion (for example: stage)"
    echo "  options - any of ansible-playbook options"
    exit 1
fi

INVENTORY="$DIR/$INVENTORY_NAME.ini"
PLAYBOOK="$DIR/$PLAYBOOK_NAME.yml"

# shift arguments and save them to the variable for
# pass them to the original ansible-playbook command
for i in $(seq 2); do
    [[ ${1:0:1} = "-" ]] && break
    [[ $1 != "" ]] && shift 1
done
args=$@

# if encrypted vault exists we have to ask a password
# or grab it from the environment variable
if [[ -f "$DIR/vault.yml" ]]; then
    if [[ $ANSIBLE_VAULT_PASSWORD != "" ]]; then
        export ANSIBLE_VAULT_PASSWORD_FILE="$DIR/utils/get-vault-password.sh"
    fi
    if [[ $args != *vault* && $ANSIBLE_VAULT_PASSWORD_FILE = "" ]]; then
        args+=" --ask-vault-pass"
    fi
fi

echo "Executing ansible-playbook $PLAYBOOK -i $INVENTORY $args"
ansible-playbook $PLAYBOOK -i $INVENTORY $args
