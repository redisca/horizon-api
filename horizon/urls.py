from django.urls import path, include
from django.views.generic import TemplateView
from django.contrib import admin
from django.conf import settings
from rest_framework import routers

from .views import health_check

from project.api import ProjectViewSet, HomepageProjectViewSet
from message.api import MessageViewSet

router = routers.DefaultRouter()
router.register('homepage-projects', HomepageProjectViewSet, basename='homepage-projects')
router.register('projects', ProjectViewSet)
router.register('message', MessageViewSet)

urlpatterns = [
    path('django/', admin.site.urls),
    path('nested_admin/', include('nested_admin.urls')),
    path('api-auth/', include('rest_framework.urls')),
    path('api/', include(router.urls)),
    path('', health_check),
]


if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
